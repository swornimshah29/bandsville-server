from flask import Flask,abort,request


import pandas as pd
from random import randint
import csv,json
import socket

#you can give any name in pd

# print('IMDB formula based filtering')
# metadata=pd.read_csv('./records.csv',low_memory=False)
# print(metadata.head(0))
# C = metadata['mRating'].mean()
# print(C)
# m = metadata['mFollowers'].quantile(0.90)
# print(m)
# #m is the minimum value which should be >=m for all other movies to be listed in the charts(recommendation)
# q_movies = metadata.copy().loc[(metadata['mFollowers'] >= m) & (metadata['mType']=='panist')]
# # q_movies = metadata.copy().loc[metadata['mFollowers'] >= m]

# #we have to give the weighted ranking (value) for each movies that were selected
# print(q_movies.shape)

# def weighted_ranking(x,m=m,C=C):
#     v = x['mFollowers']
#     R = x['mRating']
#     return (v/(v+m) * R) + (m/(m+v) * C)

# q_movies['score'] = q_movies.apply(weighted_ranking, axis=1)
# q_movies = q_movies.sort_values('score', ascending=False)
# #by default the result will be in the ascending order soo making it false

# print(len(q_movies))
# print(q_movies[['mId', 'mFollowers', 'mRating', 'score','mType','mLocation']].head(5))
print('bands recommendation for the event organizer')


class Bands:
    bandsContainer=[]
    bandName=[]

    def __init__(self,bId,bName,bMembers,bRating,bLogo,bType,bFans):
        self.bId=bId
        self.bName=bName
        self.bMembers=bMembers
        self.bRating=bRating
        self.bLogo=bLogo
        self.bType=bType
        self.bFans=bFans

    @classmethod
    def addBands(cls,object):
        cls.bandsContainer.append(object)

    @classmethod
    def createJSON(cls):
        with open('./metal_bands_2017.csv','r') as file:
            reader=csv.DictReader(file)
            for row in reader:
                eachBand=Bands(
                    row['bandId'],
                    row['band_name'],
                    None,
                    randint(3,10),
                    None,
                    row['style'],
                    row['fans']
                )
                cls.bandsContainer.append(eachBand)

        json_string=json.dumps([ob.__dict__ for ob in cls.bandsContainer])
        print(type(json_string))
        records=json.loads(json_string)
        #records type list having dictionary as each item
        print(type(json.loads(json_string)))
        with open('./bands.csv','w') as file:
            writer=csv.writer(file)
            #write each row
            writer.writerow(records[0].keys())#get the headers
            for eachBand in records:
                writer.writerow(eachBand.values())#prints the value as per the header





metadata=pd.read_csv('./bands.csv',low_memory=False)
print(metadata.head(0))
C = metadata['bRating'].mean()
print(C)
m = metadata['bFans'].quantile(0.90)#90th percentile
print(m)
#m is the minimum value which should be >=m for all other movies to be listed in the charts(recommendation)

q_movies = metadata.copy().loc[metadata['bFans'] >= m]
#we have to give the weighted ranking (value) for each movies that were selected
print(q_movies.shape)


def weighted_ranking(x,m=m,C=C):
    v = x['bFans']
    R = x['bRating']
    return (v/(v+m) * R) + (m/(m+v) * C)

q_movies['score'] = q_movies.apply(weighted_ranking, axis=1)
q_movies = q_movies.sort_values('score', ascending=False)
#by default the result will be in the ascending order soo making it false

print('top 15 bands are')
print(q_movies[['bId', 'bName', 'bRating', 'score']].head(5))
print('size ',len(Bands.bandsContainer))


app=Flask(__name__)



@app.route('/')
def index():
    Bands.createJSON()
    print('data is ', json.dumps([ob.__dict__ for ob in Bands.bandsContainer]))
    print(Bands.bandsContainer[0])
    return json.dumps(Bands.bandsContainer[0].__dict__)
    
    return 'json response was huge for the client to get network latency'

@app.route('/registration',methods=['POST'])
def registration():
    print('request data',request.json)
    return 'Your are on the registation page'



if __name__== "__main__":
    app.run('192.168.1.111','5000',debug=True)
    #start the server



