
import pandas as pd
#you can give any name in pd


metadata=pd.read_csv('./movies_metadata.csv',low_memory=False)
print(metadata.head(0))
C = metadata['vote_average'].mean()
print(C)
m = metadata['vote_count'].quantile(0.90)
print(m)
#m is the minimum value which should be >=m for all other movies to be listed in the charts(recommendation)

q_movies = metadata.copy().loc[metadata['vote_count'] >= m]
#we have to give the weighted ranking (value) for each movies that were selected
print(q_movies.shape)

def weighted_ranking(x,m=m,C=C):
    v = x['vote_count']
    R = x['vote_average']
    return (v/(v+m) * R) + (m/(m+v) * C)

q_movies['score'] = q_movies.apply(weighted_ranking, axis=1)
q_movies = q_movies.sort_values('score', ascending=False)
#by default the result will be in the ascending order soo making it false

print(q_movies[['title', 'vote_count', 'vote_average', 'score']].head(15))

