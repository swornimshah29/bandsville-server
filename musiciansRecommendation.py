
import pandas as pd
#you can give any name in pd

print('IMDB formula based filtering')
metadata=pd.read_csv('./records.csv',low_memory=False)
print(metadata.head(0))
C = metadata['mRating'].mean()
print(C)
m = metadata['mFollowers'].quantile(0.90)
print(m)
#m is the minimum value which should be >=m for all other movies to be listed in the charts(recommendation)
q_movies = metadata.copy().loc[(metadata['mFollowers'] >= m) & (metadata['mType']=='drummer')]
# q_movies = metadata.copy().loc[metadata['mFollowers'] >= m]

#we have to give the weighted ranking (value) for each movies that were selected
print(q_movies.shape)

def weighted_ranking(x,m=m,C=C):
    v = x['mFollowers']
    R = x['mRating']
    return (v/(v+m) * R) + (m/(m+v) * C)

q_movies['score'] = q_movies.apply(weighted_ranking, axis=1)
q_movies = q_movies.sort_values('score', ascending=False)
#by default the result will be in the ascending order soo making it false

print(len(q_movies))
print(q_movies[['mId', 'mFollowers', 'mRating', 'score','mType','mLocation']].head(15))

